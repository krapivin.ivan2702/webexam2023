import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import router from '@/routers'
import vuetify from './plugins/vuetify'
import store from './store'

Vue.use(VueRouter)

new Vue({
  render: h => h(App),
  el: '#app',
  vuetify,
  router,
  store,
})