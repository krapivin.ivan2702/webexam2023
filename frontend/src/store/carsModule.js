import axios from "axios"

export default {
    // в состоянии храним данные
    state: {
        cars: []
    },
    // функции, которые как-то должны менять состояния
    mutations: {
        setCars(state, payload) {
            state.cars = payload
        }
    },
    // аналог computed свойств, возвращают какие-то вычисления
    getters: {
        getAllCars(state) {
            return state.cars
        }
    },
    // функции, работающие с внешним апи
    actions: {
        getCars(context) {
            axios.get('http://127.0.0.1:8000/api/Cars/')
                .then(response => context.commit("setCars", response.data))
        }
    },
}