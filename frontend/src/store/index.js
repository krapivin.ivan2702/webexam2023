import Vue from "vue"
import Vuex from 'vuex'
import carsModule from "@/store/carsModule"

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        carsModule,
    }
})









