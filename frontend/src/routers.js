import VueRouter from 'vue-router'

import Cars from '@/pages/Cars'
import News from '@/pages/News'
import NewsItem from '@/pages/NewsItem'
import CarsItem from '@/pages/CarsItem'

export default new VueRouter({
    mode: 'history',
    routes: [{
            path: '/',
            name: 'cars',
            component: Cars
        },
        {
            path: '/cars/:id',
            name: 'carsItem',
            component: CarsItem,
            props: true,
        },
        {
            path: '/news/:id',
            name: 'newsItem',
            component: NewsItem,
            props: true,
        },
        {
            path: '/news',
            name: 'news',
            component: News,
        },

    ]
})