from rest_framework.viewsets import ModelViewSet
from brands.serializers import BrandSerializer
from brands.models import Brand

class BrandViewSet(ModelViewSet):
    queryset = Brand.objects.all()
    serializer_class = BrandSerializer
