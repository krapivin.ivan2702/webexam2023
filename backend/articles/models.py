from django.db import models

class Article(models.Model):
    title = models.CharField(verbose_name='Заголовок', max_length=255)
    short_description = models.TextField(verbose_name='Краткое описание', default='Краткое описание')
    text = models.TextField(verbose_name='Текст')

    def __str__(self):
        return self.title


    class Meta:
        verbose_name = 'Статья'
        verbose_name_plural = 'Статьи'






