from django.contrib import admin
from articles.models import Article

class MyModelAdmin(admin.ModelAdmin):
    search_fields = ['title']

admin.site.register(Article, MyModelAdmin)


