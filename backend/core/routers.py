from rest_framework.routers import DefaultRouter, SimpleRouter

from cars.views import AdViewSet
from articles.views import ArticleViewSet
from authentication.views import UserViewSet
from brands.views import BrandViewSet
from models.views import ModelViewSet
from userMessages.views import MessageViewSet

router = DefaultRouter()

router.register('Cars', AdViewSet)
router.register('Articles', ArticleViewSet)
router.register('Users', UserViewSet)
router.register('Brands', BrandViewSet)
router.register('Models', ModelViewSet)
router.register('Messages', MessageViewSet)

