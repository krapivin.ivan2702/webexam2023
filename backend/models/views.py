from rest_framework.viewsets import ModelViewSet
import django_filters
from models.serializers import ModelSerializer
from django_filters.rest_framework import DjangoFilterBackend
from models.models import Model


# filtering
class ModelsFilter(django_filters.FilterSet):
    class Meta:
        model = Model
        fields = ['name']

class ModelViewSet(ModelViewSet):
    queryset = Model.objects.all()
    serializer_class = ModelSerializer

    filter_backends = [DjangoFilterBackend]
    filterset_class = ModelsFilter







