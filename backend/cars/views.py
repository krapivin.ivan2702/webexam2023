from rest_framework.viewsets import ModelViewSet
from cars.serializers import AdSerializer
from rest_framework.pagination import PageNumberPagination

from django_filters.rest_framework import DjangoFilterBackend
from django_filters import rest_framework as filters
from rest_framework import filters

from cars.models import Car
from django.db.models import Q

# filtering
# class CarFilter(filters.FilterSet):
#     cost = filters.RangeFilter()
#     year_of_issue = filters.RangeFilter()

#     class Meta:
#         model = Car
#         fields = ['cost', 'year_of_issue']

class AdPagination (PageNumberPagination):
    page_size = 6
    page_size_query_param = 'page_size'
    max_page_size=10000

class AdViewSet(ModelViewSet):
    queryset = Car.objects.all()
    serializer_class = AdSerializer
    pagination_class = AdPagination
    filter_backends = (DjangoFilterBackend, filters.SearchFilter)
    # filterset_class = CarFilter
    search_fields = ['brand_id__name', 'model_id__name']

    # по get
    def get_queryset(self):
        queryset = Car.objects.all()
        # queryset = Car.objects.filter(Q(color = 'белый') | Q(description__startswith='А'))
        cost = self.request.query_params.get('cost', None)
        if cost is not None:
            queryset = queryset.filter(cost=cost)
        return queryset



