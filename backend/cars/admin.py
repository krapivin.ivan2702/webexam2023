from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from modeltranslation.admin import TranslationAdmin
from cars.models import Car

from django.utils.html import format_html

# class MyModelAdmin(admin.ModelAdmin):
#     search_fields = ['title']
#     list_filter = ['year_of_issue']
#     list_display = ('title', 'image')
#     filter_horizontal = ('model_id', 'brand_id')

#     def image(self,obj):
#         try:
#             return format_html('<img src="{0}" style="width: 68px; height: 45px; object-fit: cover;" />'.format(obj.photo.url))
#         except:
#             return format_html('<strong>Изображения нет</strong>')

# Экспорт excel
class CarResource(resources.ModelResource):
    class Meta:
        model = Car
        exclude = ('title', 'color', 'description')

class CarAdmin(TranslationAdmin, ImportExportModelAdmin, admin.ModelAdmin):
    search_fields = ['title']
    list_filter = ['year_of_issue']
    list_display = ('title', 'image')
    filter_horizontal = ('model_id', 'brand_id')

    def image(self,obj):
        try:
            return format_html('<img src="{0}" style="width: 68px; height: 45px; object-fit: cover;" />'.format(obj.photo.url))
        except:
            return format_html('<strong>Изображения нет</strong>')

    resource_classes = [CarResource]

# admin.site.register(Car, MyModelAdmin)
# admin.site.register(Car, SimpleHistoryAdmin)
admin.site.register(Car, CarAdmin)
