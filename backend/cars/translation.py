from modeltranslation.translator import register, TranslationOptions
from cars.models import Car

@register(Car)
class CarTranslationOptions(TranslationOptions):
    fields = ('title', 'color', 'description')